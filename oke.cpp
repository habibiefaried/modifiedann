/******************************************************************
 * ArduinoANN - An artificial neural network for the Arduino
 * All basic settings can be controlled via the Network Configuration
 * section.
 * See robotics.hobbizine.com/arduinoann.html for details.
 ******************************************************************/

#include <iostream>
#include <cstdlib>
#include <math.h>

using namespace std;

/******************************************************************
 * Network Configuration - customized per network 
 ******************************************************************/

void toTerminal();
void readSensor();

const int MaxAllocation = 200;

int PatternCount = 10;

const int InputNodes = 4;
const int HiddenNodes = 5;
const int OutputNodes = 1;
const float LearningRate = 0.3;
const float Momentum = 0.9;
const float InitialWeightMax = 0.5;
const float Success = 0.0004;

int Input[MaxAllocation][InputNodes] = {
  { 6, 3, 8, 1 },  // 0
  { 5, 3, 4, 3 },  // 1
  { 1, 1, 0, 1 },  // 2
  { 1, 1, 1, 1 },  // 3
  { 0, 1, 1, 0 },  // 4
  { 1, 0, 1, 1 },  // 5
  { 0, 0, 1, 1 },  // 6
  { 1, 1, 1, 0 },  // 7 
  { 1, 1, 1, 1 },  // 8
  { 1, 1, 1, 0 }   // 9
}; 

int Target[MaxAllocation][OutputNodes] = {
  { 0 },  
  { 1 }, 
  { 0 }, 
  { 0 }, 
  { 0 }, 
  { 0 }, 
  { 0 }, 
  { 0 }, 
  { 1 }, 
  { 1 } 
};

/******************************************************************
 * End Network Configuration
 ******************************************************************/


int i, j, p, q, r;
int ReportEvery1000;
int RandomizedIndex[MaxAllocation];
long  TrainingCycle;
float Rando;
float Error;
float Accum;


float Hidden[HiddenNodes];
float Output[OutputNodes];
float HiddenWeights[InputNodes+1][HiddenNodes];
float OutputWeights[HiddenNodes+1][OutputNodes];
float HiddenDelta[HiddenNodes];
float OutputDelta[OutputNodes];
float ChangeHiddenWeights[InputNodes+1][HiddenNodes];
float ChangeOutputWeights[HiddenNodes+1][OutputNodes];

void setup(){
  //Serial.begin(9600);
  //randomSeed(analogRead(3));
  ReportEvery1000 = 1;
  for( p = 0 ; p < PatternCount ; p++ ) {    
    RandomizedIndex[p] = p ;
  }
  cout<<"Setup Selesai"<<endl;
}  

void loop (){


/******************************************************************
* Initialize HiddenWeights and ChangeHiddenWeights 
******************************************************************/
  for( i = 0 ; i < HiddenNodes ; i++ ) {    
    for( j = 0 ; j <= InputNodes ; j++ ) { 
      ChangeHiddenWeights[j][i] = 0.0 ;
      //Rando = float(random(100))/100;
      int RR = rand() % 100;
      Rando = float(RR) / 100;
      HiddenWeights[j][i] = 2.0 * ( Rando - 0.5 ) * InitialWeightMax ;
    }
  }
/******************************************************************
* Initialize OutputWeights and ChangeOutputWeights
******************************************************************/
  for( i = 0 ; i < OutputNodes ; i ++ ) {    
    for( j = 0 ; j <= HiddenNodes ; j++ ) {
      ChangeOutputWeights[j][i] = 0.0 ;  
      //Rando = float(random(100))/100;
      int RR = rand() % 100;
      Rando = float(RR) / 100;       
      OutputWeights[j][i] = 2.0 * ( Rando - 0.5 ) * InitialWeightMax ;
    }
  }
  //Serial.println("Initial/Untrained Outputs: ");
  toTerminal();
/******************************************************************
* Begin training 
******************************************************************/
  for( TrainingCycle = 1 ; TrainingCycle < 2147483647 ; TrainingCycle++) {    

/******************************************************************
* Randomize order of training patterns
******************************************************************/

    for( p = 0 ; p < PatternCount ; p++) {
      //q = random(PatternCount);
      q = rand() % PatternCount;
      r = RandomizedIndex[p]; 
      RandomizedIndex[p] = RandomizedIndex[q] ; 
      RandomizedIndex[q] = r ;
    }
    Error = 0.0 ;
/******************************************************************
* Cycle through each training pattern in the randomized order
******************************************************************/
    for( q = 0 ; q < PatternCount ; q++ ) {    
      p = RandomizedIndex[q];

/******************************************************************
* Compute hidden layer activations
******************************************************************/
      for( i = 0 ; i < HiddenNodes ; i++ ) {    
        Accum = HiddenWeights[InputNodes][i] ;
        for( j = 0 ; j < InputNodes ; j++ ) {
          Accum += Input[p][j] * HiddenWeights[j][i] ;
        }
        Hidden[i] = 1.0/(1.0 + exp(-Accum)) ;
      }

/******************************************************************
* Compute output layer activations and calculate errors
******************************************************************/
      for( i = 0 ; i < OutputNodes ; i++ ) {    
        Accum = OutputWeights[HiddenNodes][i] ;
        for( j = 0 ; j < HiddenNodes ; j++ ) {
          Accum += Hidden[j] * OutputWeights[j][i] ;
        }
        Output[i] = 1.0/(1.0 + exp(-Accum)) ;   
        OutputDelta[i] = (Target[p][i] - Output[i]) * Output[i] * (1.0 - Output[i]) ;   
        Error += 0.5 * (Target[p][i] - Output[i]) * (Target[p][i] - Output[i]) ;
      }

/******************************************************************
* Backpropagate errors to hidden layer
******************************************************************/
      for( i = 0 ; i < HiddenNodes ; i++ ) {    
        Accum = 0.0 ;
        for( j = 0 ; j < OutputNodes ; j++ ) {
          Accum += OutputWeights[i][j] * OutputDelta[j] ;
        }
        HiddenDelta[i] = Accum * Hidden[i] * (1.0 - Hidden[i]) ;
      }


/******************************************************************
* Update Inner-->Hidden Weights
******************************************************************/
      for( i = 0 ; i < HiddenNodes ; i++ ) {     
        ChangeHiddenWeights[InputNodes][i] = LearningRate * HiddenDelta[i] + Momentum * ChangeHiddenWeights[InputNodes][i] ;
        HiddenWeights[InputNodes][i] += ChangeHiddenWeights[InputNodes][i] ;
        for( j = 0 ; j < InputNodes ; j++ ) { 
          ChangeHiddenWeights[j][i] = LearningRate * Input[p][j] * HiddenDelta[i] + Momentum * ChangeHiddenWeights[j][i];
          HiddenWeights[j][i] += ChangeHiddenWeights[j][i] ;
        }
      }

/******************************************************************
* Update Hidden-->Output Weights
******************************************************************/
      for( i = 0 ; i < OutputNodes ; i ++ ) {    
        ChangeOutputWeights[HiddenNodes][i] = LearningRate * OutputDelta[i] + Momentum * ChangeOutputWeights[HiddenNodes][i] ;
        OutputWeights[HiddenNodes][i] += ChangeOutputWeights[HiddenNodes][i] ;
        for( j = 0 ; j < HiddenNodes ; j++ ) {
          ChangeOutputWeights[j][i] = LearningRate * Hidden[j] * OutputDelta[i] + Momentum * ChangeOutputWeights[j][i] ;
          OutputWeights[j][i] += ChangeOutputWeights[j][i] ;
        }
      }
    }

/******************************************************************
* Every 1000 cycles send data to terminal for display
******************************************************************/
    ReportEvery1000 = ReportEvery1000 - 1;
    if (ReportEvery1000 == 0)
    {
      //Serial.println(); 
      //Serial.println(); 
      //Serial.print ("TrainingCycle: ");
      //Serial.print (TrainingCycle);
      //Serial.print ("  Error = ");
      //Serial.println (Error, 5);

      toTerminal();

      if (TrainingCycle==1)
      {
        ReportEvery1000 = 999;
      }
      else
      {
        ReportEvery1000 = 1000;
      }
    }    


/******************************************************************
* If error rate is less than pre-determined threshold then end
******************************************************************/
    if( Error < Success ) break ;  
  }
  //Serial.println ();
  //Serial.println(); 
  //Serial.print ("TrainingCycle: ");
  //Serial.print (TrainingCycle);
  //Serial.print ("  Error = ");
  //Serial.println (Error, 5);

  toTerminal();

  //Serial.println ();  
  //Serial.println ();
  //Serial.println ("Training Set Solved! ");
  //Serial.println ("--------"); 
  //Serial.println ();
  //Serial.println ();  
  ReportEvery1000 = 1;
}

void toTerminal()
{

  for( p = 0 ; p < PatternCount ; p++ ) { 
    cout<<endl;
    cout<<"Training Pattern: "<<endl;
    cout<<p<<" "<<endl;      
    cout<<"Input : "<<endl;
    
    for( i = 0 ; i < InputNodes ; i++ ) {
       cout<<Input[p][i]<<" ";
    }
    cout<<endl;
    cout<<"Target"<<endl;
    for( i = 0 ; i < OutputNodes ; i++ ) {
      cout<<Target[p][i]<<" ";
    }
/******************************************************************
* Compute hidden layer activations
******************************************************************/

    for( i = 0 ; i < HiddenNodes ; i++ ) {    
      Accum = HiddenWeights[InputNodes][i] ;
      for( j = 0 ; j < InputNodes ; j++ ) {
        Accum += Input[p][j] * HiddenWeights[j][i] ;
      }
      Hidden[i] = 1.0/(1.0 + exp(-Accum)) ;
    }

/******************************************************************
* Compute output layer activations and calculate errors
******************************************************************/

    for( i = 0 ; i < OutputNodes ; i++ ) {    
      Accum = OutputWeights[HiddenNodes][i] ;
      for( j = 0 ; j < HiddenNodes ; j++ ) {
        Accum += Hidden[j] * OutputWeights[j][i] ;
      }
      Output[i] = 1.0/(1.0 + exp(-Accum)) ; 
    }
    cout<<endl;
    cout<<"Output "<<endl;
    for( i = 0 ; i < OutputNodes ; i++ ) {       
      cout<<Output[i]<<"  ";
    }
  }


}

void readSensor(int a1, int a2, int a3, int a4, int T) {
  Input[PatternCount][0] = a1;
  Input[PatternCount][1] = a2;
  Input[PatternCount][2] = a3;
  Input[PatternCount][3] = a4;
  Target[PatternCount][0] = T;
  PatternCount++;
}

//Tambahan buat bahasa C++
int main(int argc, char** argv) {
  readSensor(0,1,3,2,1);
	setup();
	loop();
	return 0;
}